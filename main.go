package main

import (
	"fmt"
	"math"
	"strings"
	"bytes"
	"os"
	"io/ioutil"
	"path/filepath"
	"container/list"
	"sort"
	"hash/crc32"
	"io"
	"net"
	"encoding/gob"
	"net/http"
)

func main() {
	// http server, handle url by a function
	http.Handle("/hello", HelloHandler{})
	http.ListenAndServe(":9000", nil)

	// tcp server and client
	go server()
	go client()
	// hash a file
	h1_int, _ := getHash("/Users/kaiyin/GolangProjects/intro-go/pic.png")
	h1_hex := fmt.Sprintf("%#x", h1_int)
	fmt.Println(h1_hex)
	fmt.Println(h1_hex == "0xf804fb71")

	// hash functions
	h := crc32.NewIEEE()
	h.Write([]byte("test"))
	// calculate crc32 checksum
	fmt.Println(h.Sum32())
	// sort
	kids := []Person{
		{"Jill", 32},
		{"Bane", 3},
		{"Anna", 132},
		{"Minis", 31},
		{"Nano", 37},
	}
	sort.Sort(ByName(kids))
	fmt.Println(kids)
	sort.Sort(ByAge(kids))
	fmt.Println(kids)
	// doubly linked list
	var myList  = list.List{}
	for i := 0; i < 4; i++ {
		myList.PushBack(i)
	}
	for e := myList.Front(); e != nil; e = e.Next() {
		fmt.Println(e.Value.(int))
	}
	// walk a path
	filepath.Walk(".", func(path string, info os.FileInfo, err error) error {
		fmt.Println(path)
		return nil
	})
	// read contents of a directory
	dir, error00 := os.Open(".")
	if error00 != nil {
		return
	}
	defer dir.Close()
	fileInfos, error01 := dir.Readdir(-1)
	if error01 != nil {
		return
	}
	for _, fi := range fileInfos {
		fmt.Println(fi.Name())
	}
	filename := "/tmp/test.txt"
	// write to file
	file, error0 := os.Create(filename)
	if error0 != nil {
		return
	}
	defer file.Close()
	file.WriteString("what\nis\nthis?\n")
	// read a file, the long way
	file, err := os.Open(filename)
	if err != nil {
		return
	}
	defer file.Close()
	stat, err := file.Stat()
	if err != nil {
		return
	}
	bs := make([]byte, stat.Size())
	_, err = file.Read(bs)
	if err != nil {
		return
	}
	// read a file, the short way
	bs1, err1 := ioutil.ReadFile(filename)
	if err1 != nil {
		return
	}
	fmt.Println(string(bs1))
	str := string(bs)
	fmt.Println(str)

	// string methods
	fmt.Println(strings.HasPrefix("hello", "hell"))
	fmt.Println(strings.HasSuffix("world", "ld"))
	fmt.Println(strings.Index("there", "re"))
	fmt.Println(strings.Join([]string{"4314", "870954", "731"}, "-"))
	fmt.Println(strings.Repeat("a", 15))
	fmt.Println(strings.Replace("aaaa", "a", "b", 2))
	fmt.Println(strings.Split("a-b-c-d", "-"))

	// buffer writer
	var buf bytes.Buffer
	buf.Write([]byte("test"))
	fmt.Println(buf)

	// composition acting like inheritance
	a := new(Android)
	a.Person = Person{"Jan", 45}
	a.Talk() // the Talk method comes from Person, not Android

	// nested data structure
	ms := MultiShape{
		shapes: []Shape{
			Rectangle{Point{1.0, 1.0}, Point{2.0, 2.0}},
			Rectangle{Point{2.0, 2.0}, Point{3.0, 3.0}},
			Rectangle{Point{3.0, 3.0}, Point{4.0, 4.0}},
			MultiShape{
				shapes: []Shape{
					Rectangle{Point{1.0, 1.0}, Point{2.0, 2.0}},
					Rectangle{Point{2.0, 2.0}, Point{3.0, 3.0}},
					Rectangle{Point{3.0, 3.0}, Point{4.0, 4.0}},
				},
			},
		},
	}
	fmt.Println(ms.area())
}

type Shape interface {
	area() float64
}

type MultiShape struct {
	shapes []Shape
}

func (ms MultiShape) area() float64 {
	total := 0.0
	for _, shape := range ms.shapes {
		total += shape.area()
	}
	return total
}

type Point struct {
	x, y float64
}

func distance(p1, p2 Point) float64 {
	return math.Sqrt(
		math.Pow(p1.x - p2.x, 2.0) +
			math.Pow(p1.y - p2.y, 2.0),
	)
}

type Circle struct {
	p Point
	r float64
}

type Rectangle struct {
	upperLeft, bottomRight Point
}

type Person struct {
	Name string
	Age int
}

func (p Person) Talk() {
	fmt.Println("Hi, my name is ", p.Name)
}

type Android struct {
	Person
	Model string
}

func (r Rectangle) area() float64 {
	w := math.Abs(r.bottomRight.x - r.upperLeft.x)
	l := math.Abs(r.bottomRight.y - r.upperLeft.y)
	return w * l
}

func (c Circle) area() float64 {
	return math.Pi * c.r * c.r
}



func makeEvenGenerator() func() uint {
	i := uint(0)
	return func() (res uint) {
		res = i
		i += 2
		return
	}
}

func factorial(x uint) uint {
	if x == 0 {
		return 1
	}
	return x * factorial(x - 1)
}

func zero(xPtr *int) {
	*xPtr = 0
}

func one(xPtr *int) {
	*xPtr = 1
}

type ByName []Person

func (ps ByName) Len() int {
	return len(ps)
}

func (ps ByName) Less(i, j int) bool {
	return ps[i].Name < ps[j].Name
}

func (ps ByName) Swap(i, j int) {
	ps[i], ps[j] = ps[j], ps[i]
}

type ByAge []Person
func (ps ByAge) Len() int {
	return len(ps)
}

func (ps ByAge) Less(i, j int) bool {
	return ps[i].Age < ps[j].Age
}

func (ps ByAge) Swap(i, j int) {
	ps[i], ps[j] = ps[j], ps[i]
}


func getHash(filename string) (uint32, error) {
	f, err := os.Open(filename)
	if err != nil {
		return 0, err
	}
	defer f.Close()
	h := crc32.NewIEEE()
	_, err = io.Copy(h, f)
	if err != nil {
		return 0, nil
	}
	return h.Sum32(), nil
}

// tcp server
func server() {
	ln, err := net.Listen("tcp", ":9999")
	if err != nil {
		fmt.Println(err)
		return
	}
	for {
		c, err := ln.Accept()
		if err != nil {
			fmt.Println(err)
			continue
		}
		go handleTCPConnection(c)
	}
}
func handleTCPConnection(c net.Conn) {
	var msg string
	err := gob.NewDecoder(c).Decode(&msg)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("Received", msg)
	}
	defer c.Close()
}

func client() {
	c, err := net.Dial("tcp", "127.0.0.1:9999")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer c.Close()
	msg := "Hello"
	fmt.Println("Sending", msg)
	err = gob.NewEncoder(c).Encode(msg)
	if err != nil {
		fmt.Println(err)
	}
}

type HelloHandler struct {}

func (_ HelloHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	res.Header().Set("Content-Type", "text/html")
	io.WriteString(res, `
	<DOCTYPE html>
	<html>
	<head>
	<title>Hello, world!</title>
	</head>
	<body>Hello, world!</body>
	</html>
	`)
}
