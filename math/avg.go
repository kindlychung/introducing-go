package math

func average(xs []float64) float64 {
	add := func(args ...float64) float64 {
		total := 0.0
		for _, x := range args {
			total += x
		}
		return total
	}
	return add(xs...) / float64(len(xs))
}
