package math

import "testing"

type TestPair struct {
	values []float64
	average float64
}

var tests = []TestPair {
	{[]float64{1, 2, 3}, 2.0},
	{[]float64{1, 2, 3, 4}, 2.5},
	{[]float64{1, 2, 3, 4, 5}, 3.0},
}

func TestAverage(t *testing.T) {
	for _, pair := range tests {
		got := average(pair.values)
		if  got != pair.average {
			t.Error(
				"Average of input: ", pair.values,
				"Expected: ", pair.average,
				"Got: ", got,
			)
		}
	}
}
